from os import path

from setuptools import find_namespace_packages, setup

VERSION = "2.1.0.dev"


this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, "README.md"), encoding="utf-8") as f:
    long_description = f.read()

setup(
    name="NEMO-group-email",
    version=VERSION,
    python_requires=">=3.7",
    description="Install Group email plugin for NEMO",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Atlantis Labs LLC",
    author_email="atlantis@atlantislabs.io",
    url="https://gitlab.com/nemo-community/atlantis-labs/nemo-group-email",
    packages=find_namespace_packages(),
    include_package_data=True,
    install_requires=[
        "NEMO>=4.3.0",
        "NEMO-user-details>=1.0.0",
        "django",
    ],
    license="MIT",
    keywords=["NEMO"],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.7",
    ],
)
